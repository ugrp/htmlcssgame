# html css game

This game is made out of html links, <a href="chapters/intro">like this one</a>.

The animations and styling are made of css.

Each link bring to a new page, or to the same page with a different
state.

If you are stuck in the game, and don't know how to go to the next
step, just increment the page number in the url of the browser in
which you are playing.

## Objectives

The idea is just to experiment with how to make a game with the
simplest tools available, an `index.html` file.

Also it is an interesting place to get started looking at what html
and css are, by just inspecting the code of any of the pages of the
game.

## Create game levels

It is absolutely possible to create your own game, and even integrate
them to this version. It is accessible to any one willing to practise
and learn an introduction of html and css.

In the this project's folder, there is a `./chapters` folder, within which
are organized each levels, in a level folder.

A level folder is named after the number of the level, there can only
be one level with the same number, onle level 1, one level 2 etc.

Copy the folder of any game level, and clean existing styles to
get started.

You have a full blank page of freedom on what game level and design to
implements.

The only rules of creating game levels are:

- only use html and css (you could also import files from other level folders)
- refraign using links to external URLs

Of course you can use any valid html markup, put blocks over an other,
move rotate etc, fake. anything!

Have fun.
